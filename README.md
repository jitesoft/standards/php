# PHP Standard for Jitesoft organization

The Jitesoft PHP standard is quite like the PSR-2 coding standard although not 100% compatible.
If something is not explicitly defined in this description, it follows the PSR-2 standard.

_This file use a lot of the structure and some of the text used in the [PSR-2](https://www.php-fig.org/psr/psr-2/) standard. 
The [FIG](https://github.com/php-fig/fig-standards) standards uses the CCA license which can be 
found [here](https://github.com/php-fig/fig-standards/blob/master/LICENSE-CC.md).
PSR-2 is attributed to Paul M. Jones._

Included in this repository is a style file (`phpstyle.xml`) to be used with [PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer), 
if any rules are mismatched with the below defined rules, please update the code sniffer file or talk to a maintainer.

---

## Standard

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in RFC 2119.

### Overview

* Code MUST use 4 spaces for indenting, no tabs.
* There MUST be a hard limit on line length at 120 characters.
* Line lengths SHOULD be 80 characters or less.
* There MUST be a blank line after `namespace` declarations and after `use` declarations.
* Opening braces on classes MUST go on the same line as the class declaration.
* There MUST be a blank line before closing class brace.
* Visibility MUST be declared on all properties and methods.
* `abstract`, `final` `const` and `static` keywords MUST be declared after the visibility.
* Control structure keywords MUST have one space after them.
* Method and Function calls MUST NOT have space between keyword and parentheses.
* Opening braces for control structures MUST go on the same line.
* Closing braces for control structures MUST go on the next line after body.
* Opening parentheses for control structures MUST have a space after them.
* Closing parentheses for control structures MUST NOT have a space before.

Example: 

```php
<?php
namespace Jitesoft\Package\Name;

use Jitesoft\OtherPackage\InterfaceName;
use Jitesoft\OtherPackage\ClassName;
use OtherVendor\Package\ClassName as SomeClass;

abstract class MyClass extends ClassName implements InterfaceName {
    public const A_CONSTANT = 'abc';

    public static $staticClass = null;

    private $class = null;

    public function __constructor(SomeClass $class) {
        $this->class = $class;
        self::$staticClass = $class;       
    }
    
    public abstract function abstractMethod(int $aInt): ?string;
    
    public static function staticMethod() {
        
    }
    
    protected function protectedMethod(): void {
        for ($i = 0; $i > 10; $i++) {
            if ($i === 2) {
                self::staticMethod();                   
            } else if ($i === 3) {
                $this->abstractMethod();                
            } else {
                $this->class($i);                
            }
        }
    }
}

```

### General

#### Files

Files must use LF line endings.  
PHP files MUST end with a single blank line.  
The closing tag (`?>`) MUST not be used in files containing only PHP.

#### Lines

Line length MUST have a hard limit at 120 characters.  
Line length MAY have a soft limit at 80 characters.  
There MUST be no blank spaces at EOL.  
There MUST NOT be more than one statement per line.

#### Indenting

There MUST be 4 space indents.  
There MUST NOT be tabs for indents.

#### Keywords

Keywords MUST be lower case.

### Namespaces and Use declarations

All files with only code MUST have a namespace.  
All namespaces MUST use `Jitesoft` as parent namespace.  
All namespaces MUST use `PascalCase` convention.  
There MUST be a blank line between namespace declaration and underlying code block.  
All `use` declarations MUST be after `namespace` declaration and before next code block.  
There MUST be a blank line after the `use` block.  
Namespaces MUST NOT be more than 2 depths stacked.

Example:

```php
<?php
namespace Jitesoft\New\Package\CalledSomething\ClassName;

use Other\Package\AnotherClass;
use Other\AnotherPackage\ {
    SubPackage1\ClassName,
    SubPackage2\ClassName as Name
};

```

### Classes, Methods, Functions and Properties

The term `class` refers to (in this document) all common structures as `class`, `trait`, `interface`.  

#### Extends / Implements

The `extends` and `implement` keywords MUST be declared at the same line as the class name.  
The extended class name MUST be declared at the same line as the class name.  
The (first) implemented interface name MUST go on the same line as the class name.  
If reaching the hard line limit, interface names MAY use a line for each consecutive interface.  
Imported classes SHOULD NOT use their fully qualified name.  

#### Properties

Visibility MUST be declared on all properties.  
There MUST NOT be more than one property declared per line.  
None constant property names MUST use `camelCase` convention.  
All public properties MUST be documented with its expected type.  
Properties of the same visibility MUST be in a block with aligned values.

#### Methods

Visibility MUST be declared on all methods.  
Method names MUST use `camelCase` convention.  
The parentheses MUST NOT have a space before opening and MUST have a space after closing.  
The opening brace MUST go on the same line as the method name.  
The closing brace MUST go on the line after the body.  
There MUST be a space between code above the method declaration and after the method declaration.  
All public methods MUST be documented with expected value and return -types and a short description.

#### Method arguments

Method arguments MUST use `camelCase` convention.  
Arguments with default value MUST go last in the argument list.  
Methods MAY use the rest parameter list definition (`...$args``).  
There MUST be a space after comma and MUST NOT be a space before comma.  
Types MUST be defined if possible, else they MUST be properly documented.  
Arguments MAY go over multiple lines, where each new line is indented to the same position as the one above.  
If multiple lines is used the closing parentheses and the opening brace MUST be on the same line as the last argument.  

#### Abstract, Static and Final

When used, the `abstract`, `static` and `final` keywords MUST be after the visibility declaration.  
If used on fields, the fields with each type of keyword MUST be in a block with aligned values.

#### Constants

Constants MUST use `ALL_CAPS` format with `_` separating words.  
Constants MUST use visibility declaration.  
If multiple constant fields, they MUST be in a block with aligned values.

#### Method and Function calls

Method and function calls MUST NOT have a space between method name and parentheses.  
There MUST NOT be a space between opening parentheses and first argument.  
There MUST NOT be a space between last argument and closing parentheses.  
Argument lists may use rest parameter list definition (`...$arr``).  
Argument lists MAY be split across multiple lines, if so, the first argument MUST come right after the opening parentheses
while the others MUST be indented to the same position as the first argument. The closing parentheses MUST be on its own line.

### Control structures

There MUST be one space between the control structure keyword and the opening parentheses or brace.  
There MUST NOT be a space after opening parentheses.  
There MUST be braces on all applicable control structures.  
The structure body MUST be indented once.  
There MUST be a space between closing parentheses and the opening brace.  
The closing brace MUST be on the next line after the body.  
There SHOULD be a empty new line after a control structure.
There SHOULD be a empty line before a control structure.

#### if, else and else if

The `elseif` keyword SHOULD NOT be used but rather `else if`.  

#### switch / case

The case keyword MUST be on its own line.    
The case MAY use braces.  
Cases MAY use fallthrough as long as it is properly documented.  
Switch structures MUST have a default case.

#### for

For loops SHOULD NOT use `count` in the declaration unless it is a backwards running for-loop.  
For loops MAY use simple variable names for index counters.  

#### try/catch/final

A try statement SHOULD catch each type of exception it potentially throws.  
There MUST be a space between the `try` keyword and the opening brace.  
The `catch` keywords SHOULD be on the same line as the closing brace.  
`catch` keywords MUST have a space between the keyword and opening parentheses.  
`catch` keywords MUST have a space between the closing parentheses and the opening brace.  
`finally` keyword MAY be used.  
`finally` keyword MUST be used when cleanup is required in either case.  

#### Closures

Closures MUST NOT have a space after function keyword and opening parentheses.  
The closing space MUST have a space between itself and the next entry.  
`use` keyword MUST be on the same line as the function declaration as long as the argument list is single line.  
The opening brace MUST have a space before itself.  
The closing brace MUST be on its own line.  
Arguments MAY be stacked on multiple lines, but MUST be indented to the same position as the above.  
